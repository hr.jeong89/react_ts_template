import { DARK_THEME, LIGHT_THEME, THEME_TYPES } from '@common_types/ThemeTypes'
import React, { createContext, ReactNode, useContext, useState } from 'react'

type ThemeType = DARK_THEME | LIGHT_THEME | string

export interface IThemeContextType {
  theme: ThemeType
  toggleTheme: () => void
}

export interface IThemeProviderProps {
  children: ReactNode
}

export const ThemeContext = createContext<IThemeContextType | undefined>(undefined)

export const ThemeContextProvider: React.FC<IThemeProviderProps> = ({ children }) => {
  const storageTheme: ThemeType = document.body.className
  const [theme, setTheme] = useState<ThemeType>(storageTheme ? storageTheme : THEME_TYPES.light)

  const toggleTheme = () => {
    setTheme((prevTheme) => {
      const newTheme = prevTheme === THEME_TYPES.light ? THEME_TYPES.dark : THEME_TYPES.light
      localStorage.setItem('theme', newTheme)

      return newTheme
    })
  }

  const contextValue: IThemeContextType = {
    theme,
    toggleTheme,
  }

  return <ThemeContext.Provider value={contextValue}>{children}</ThemeContext.Provider>
}

export const useTheme = (): IThemeContextType => {
  const context = useContext(ThemeContext)
  if (!context) {
    throw new Error('useTheme must be used within a ThemeProvider')
  }
  return context
}
