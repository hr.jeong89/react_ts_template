import { LangCode } from '@common_types/LocaleTypes'
import React, { createContext, ReactNode, useContext, useEffect, useMemo, useState } from 'react'

export interface ILocaleType {
  lang: string
  setLang: (lang: LangCode) => void
  strings: Record<string, string>
  isLoading: boolean
}

interface LocaleProviderProps {
  children: ReactNode
}

export const LocaleContext = createContext<ILocaleType | undefined>(undefined)

const loadResource = async (lang: string): Promise<Record<string, string>> => {
  return await import(`@locales/messages_${lang}.json`).then((module) => module.default)
}

export const LocaleContextProvider = ({ children }: LocaleProviderProps) => {
  const [lang, setLang] = useState<LangCode>('kr')
  const [strings, setStrings] = useState<Record<string, string>>({})
  const [isLoading, setIsLoading] = useState<boolean>(true)

  useEffect(() => {
    const fetchLocale = async (): Promise<void> => {
      try {
        const resource = await loadResource(lang)
        setStrings(resource)
      } catch (error) {
        console.log(`Failed to load for ${lang}`, error)
      } finally {
        setIsLoading(false)
      }
    }

    fetchLocale()
  }, [lang])

  const contextValue = useMemo(
    () => ({
      lang,
      setLang,
      strings,
      isLoading,
    }),
    [lang, setLang, strings, isLoading]
  )

  return isLoading ? null : (
    <LocaleContext.Provider value={contextValue}>{children}</LocaleContext.Provider>
  )
}

export const useLocale = (): ILocaleType => {
  const context = useContext(LocaleContext)
  if (!context) {
    throw new Error('locale context is empty!!!', context)
  }

  return context
}
