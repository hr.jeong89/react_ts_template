import { MouseEvent } from 'react'

const isDimmedClick = (e: MouseEvent<HTMLElement>, className: string) => {
  const { target } = e

  if (target instanceof HTMLElement) {
    return target.classList.contains(className)
  }

  return null
}

export { isDimmedClick }
