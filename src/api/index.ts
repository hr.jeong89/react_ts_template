import axios, { AxiosRequestConfig } from 'axios'
import configs from 'configs/config'

const REQUEST_TIMEOUT = 30000
const config = {
  baseURL: configs.BASE_URL,
  timeout: REQUEST_TIMEOUT,
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
  },
  withCredentials: false,
}

const axiosInstance = axios.create(config)

const axiosApi = {
  post<T>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
    return axiosInstance.post(url, data, config)
  },
  put<T>(url: string, config?: AxiosRequestConfig): Promise<T> {
    return axiosInstance.put(url, config)
  },
  get<T>(url: string, config?: AxiosRequestConfig): Promise<T> {
    return axiosInstance.get(url, config)
  },
  delete<T>(url: string, config?: AxiosRequestConfig): Promise<T> {
    return axiosInstance.delete(url, config)
  },
}

export { axiosApi }
