import { CountTest, LangTest, ThemeTest } from '@components/test_view'
import { AlertTest } from '@components/test_view/AlertTest'
import { useLocale } from '@contexts/LocaleContext'
import { useTheme } from '@contexts/ThemeContext'
import { useLang } from '@hooks/useLang'
import React from 'react'

export const App = () => {
  const { isLoading } = useLocale()
  const { t } = useLang()
  const { theme } = useTheme()

  if (isLoading) {
    return <div>Loading...</div>
  }

  if (theme) {
    document.body.className = theme
  }

  return (
    <div className={`app ${theme === 'dark' ? 'dark-theme' : 'light-theme'}`}>
      {t('hello')}
      <LangTest />
      <ThemeTest />
      <CountTest />
      <AlertTest />
    </div>
  )
}
