export type KR = 'kr'
export type EN = 'en'
export type JP = 'jp'

export const LOCALE_TYPES = {
  kr: 'kr',
  en: 'en',
  jp: 'jp',
}

export type LangCode = KR | EN | JP
