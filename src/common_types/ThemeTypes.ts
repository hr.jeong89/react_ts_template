type DARK_THEME = 'dark'
type LIGHT_THEME = 'light'

const THEME_TYPES = {
  dark: 'dark',
  light: 'light',
}

export { DARK_THEME, LIGHT_THEME, THEME_TYPES }
