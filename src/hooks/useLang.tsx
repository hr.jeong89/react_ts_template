import { useLocale } from '@contexts/LocaleContext'
import { useMemo } from 'react'

export const useLang = () => {
  const { strings } = useLocale()

  const t = useMemo(() => {
    return (text: string, ...argv: string[]): string => {
      const re = strings[text] || text

      if (argv.length > 0) {
        re.replace(/{(\d+)}/g, (match, i) => {
          const index = Number.parseInt(i)
          return argv.length > index ? argv[index] : ''
        })
      }

      return re
    }
  }, [strings])

  return { t }
}
