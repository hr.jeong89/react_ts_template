import { IThemeContextType, ThemeContext } from '@contexts/ThemeContext'
import { useContext } from 'react'

export const useTheme = (): IThemeContextType => {
  const context = useContext(ThemeContext)
  if (!context) {
    throw new Error('theme context not ')
  }

  return context
}
