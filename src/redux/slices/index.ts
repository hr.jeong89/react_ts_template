import { configureStore } from '@reduxjs/toolkit'

import alertReducer from './common/alert'
import countReducer from './test/count'

export const store = configureStore({
  reducer: {
    count: countReducer,
    alert: alertReducer,
  },
})

export type IRootStateType = ReturnType<typeof store.getState>
