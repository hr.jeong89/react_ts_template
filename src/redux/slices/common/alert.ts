import { createSlice, PayloadAction } from '@reduxjs/toolkit'

export interface IAlertState {
  [key: string]: boolean
}

export const ALERT = 'alert'
//const OPEN = `${ALERT}/open`
//const CLOSE = `${ALERT}/close`

//export const layerActions = {
//  open: createAction(OPEN),
//  close: createAction(CLOSE)
//}

//export const reducer = {
//  [OPEN]: (state: IAlertState, action: PayloadAction<string>) => ({
//    ...state,
//    [action.payload]: true,
//  }),
//  [CLOSE]: (state: IAlertState, action: PayloadAction<string>) => ({
//    ...state,
//    [action.payload]: false,
//  }),
//}
//
//export const alertActions = {
//  openAlert: (state: IAlertState, action: PayloadAction<string>) => ({
//    ...state,
//    [action.payload]: true,
//  }),
//  closeAlert: (state: IAlertState, action: PayloadAction<string>) => ({
//    ...state,
//    [action.payload]: false,
//  })
//}

const initialState: IAlertState = {}

const alertSlice = createSlice({
  name: ALERT,
  initialState,
  reducers: {
    openAlert: (state: IAlertState, action: PayloadAction<string>) => {
      //state[action.payload] = true
      //console.log("123123123")
      return {
        ...state,
        [action.payload]: true,
      }
    },
    closeAlert: (state: IAlertState, action: PayloadAction<string>) => ({
      ...state,
      [action.payload]: false,
    }),
  },
})

export const { openAlert, closeAlert } = alertSlice.actions

export default alertSlice.reducer
