import { IRootStateType } from '@redux/slices'
import { createSlice, PayloadAction } from '@reduxjs/toolkit'

export interface ICountState {
  count: number
}

export const initialState: ICountState = {
  count: 0,
}

const increaseAction = (state: ICountState, action: PayloadAction<number>) => {
  state.count = state.count + action.payload
}

const decreaseAction = (state: ICountState, action: PayloadAction<number>) => {
  state.count = state.count - action.payload
}

const countSlice = createSlice({
  name: 'count',
  initialState,
  reducers: {
    increase: increaseAction,
    decrease: decreaseAction,
  },
  /* 비동기 처리 */
  //  extraReducers: (builder) => {
  //  builder
  //    .addCase(countActions.increase, (state, action:PayloadAction<number>) => {
  //      state.count += action.payload
  //  }).addCase(countActions.decrease, (state, action:PayloadAction<number>) => {
  //      state.count -= action.payload
  //  })},
})

export const countSelector = {
  getCount: (state: IRootStateType): ICountState => state.count,
}

export const { increase, decrease } = countSlice.actions

export default countSlice.reducer
