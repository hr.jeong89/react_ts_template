import { LocaleContextProvider } from '@contexts/LocaleContext'
import { ThemeContextProvider } from '@contexts/ThemeContext'
import { store } from '@redux/slices'
import React from 'react'
import ReactDOM from 'react-dom/client'
import { Provider } from 'react-redux'

import { App } from './App'

window.onload = async () => {
  render()
}

function render() {
  const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement)

  root.render(
    <React.StrictMode>
      <ThemeContextProvider>
        <LocaleContextProvider>
          <Provider store={store}>
            <App />
          </Provider>
        </LocaleContextProvider>
      </ThemeContextProvider>
    </React.StrictMode>
  )
}
