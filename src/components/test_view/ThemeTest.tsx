import { useTheme } from '@contexts/ThemeContext'
import React from 'react'

export const ThemeTest = () => {
  const { toggleTheme } = useTheme()

  //const clickDark = () => {
  //  toggleTheme()
  //}

  const clickToggle = () => {
    toggleTheme()
  }

  return (
    <>
      <button onClick={clickToggle}>{'Dark/White'}</button>
    </>
  )
}
