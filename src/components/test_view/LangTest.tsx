import { useLocale } from '@contexts/LocaleContext'
import React from 'react'

export const LangTest: React.FC = () => {
  const { setLang } = useLocale()

  const clickEN = () => {
    setLang('en')
  }

  const clickKR = () => {
    setLang('kr')
  }

  return (
    <div>
      <button onClick={clickEN}>{'EN'}</button>
      <button onClick={clickKR}>{'KR'}</button>
    </div>
  )
}
