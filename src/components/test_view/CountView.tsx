import { countSelector } from '@redux/slices/test/count'
import React from 'react'
import { useSelector } from 'react-redux'

export const CountView = () => {
  const { count } = useSelector(countSelector.getCount)

  return <div>count: {count}</div>
}
