import ALERT_TYPE from '@common_types/AlertTypes'
import { CommonAlertLayer } from '@components/commons/layer/AlertLayer'
import { openAlert } from '@redux/slices/common/alert'
import React from 'react'
import { useDispatch } from 'react-redux'

export const AlertTest = () => {
  const dispatch = useDispatch()

  const onClick = () => {
    dispatch(openAlert(ALERT_TYPE.COMMON_ALERT))
  }

  return (
    <div>
      <button onClick={onClick}>alert</button>
      <CommonAlertLayer />
    </div>
  )
}
