import * as countActions from '@redux/slices/test'
import React, { MouseEvent } from 'react'
import { useDispatch } from 'react-redux'

import { CountView } from './CountView'

export const CountTest = () => {
  const dispatch = useDispatch()

  const onIncrease = (e: MouseEvent<HTMLButtonElement>, n: number) => {
    dispatch(countActions.increase(n))
  }

  const onDecrease = (e: MouseEvent<HTMLButtonElement>, n: number) => {
    dispatch(countActions.decrease(n))
  }

  return (
    <div>
      <CountView />
      <button onClick={(e) => onIncrease(e, 1)}>increase</button>
      <button onClick={(e) => onDecrease(e, 1)}>decrease</button>
    </div>
  )
}
