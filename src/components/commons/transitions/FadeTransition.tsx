import './FadeTransition.css'

import React, { ReactNode } from 'react'
import { CSSTransition } from 'react-transition-group'

interface IFadeTransitionProps {
  children: ReactNode
  in: boolean
  isMountOnEnter?: boolean
}

export const FadeTransition = (props: IFadeTransitionProps) => {
  return (
    <CSSTransition
      timeout={400}
      in={props.in}
      mountOnEnter={props.isMountOnEnter}
      classNames={'fade'}
    >
      {props.children}
    </CSSTransition>
  )
}
