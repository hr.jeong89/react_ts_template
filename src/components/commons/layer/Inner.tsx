import React, { ReactNode } from 'react'

interface IInnerProps {
  children: ReactNode
}

export const Inner = (props: IInnerProps) => {
  return <div className="dim_inner">{props.children}</div>
}
