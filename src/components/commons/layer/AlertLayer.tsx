import ALERT_TYPE from '@common_types/AlertTypes'
import * as Layer from '@components/commons/layer'
import { useLang } from '@hooks/useLang'
import { IRootStateType } from '@redux/slices'
import { ALERT, closeAlert, IAlertState } from '@redux/slices/common/alert'
import React, { ReactNode } from 'react'
import { useDispatch, useSelector } from 'react-redux'

//const modalRoot: Element = document.getElementById('layer') as Element

interface IAlertLayerProps {
  children?: ReactNode
}

export const CommonAlertLayer = (props: IAlertLayerProps) => {
  const { t } = useLang()
  const dispatch = useDispatch()
  const alertStatus = useSelector<IRootStateType, IAlertState>((state) => state[ALERT])

  const isOpen = alertStatus[ALERT_TYPE.COMMON_ALERT]

  const onCancel = () => {
    dispatch(closeAlert(ALERT_TYPE.COMMON_ALERT))
  }

  return (
    <Layer.Wrapper isOpen={isOpen} onClose={onCancel}>
      <Layer.Inner>
        {t('hello')}
        {props.children}
      </Layer.Inner>
    </Layer.Wrapper>
  )
}

export const CommonOkCancelLayer = () => {}
