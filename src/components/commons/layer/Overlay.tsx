import React, { CSSProperties, ReactNode } from 'react'

interface IOverlayProps {
  style: CSSProperties
  children: ReactNode
}

export const Overlay = (props: IOverlayProps) => {
  return (
    <div className="dim_overlay" style={props.style}>
      {props.children}
    </div>
  )
}
