import * as Layout from '@components/commons/layer'
import { isDimmedClick } from '@utils/domUtils'
import React, { MouseEvent, ReactNode } from 'react'

interface IWrapperProps {
  children: ReactNode
  isOpen: boolean
  onClose: () => void
}

export const Wrapper = (props: IWrapperProps) => {
  const dimClassName = 'dim'

  const handleClick = (e: MouseEvent<HTMLDivElement>) => {
    if (isDimmedClick(e, dimClassName)) {
      props.onClose()
    }
  }

  return (
    <Layout.Overlay style={{ display: props.isOpen ? 'block' : 'none' }}>
      <div className={dimClassName} onClick={handleClick}>
        {props.children}
      </div>
    </Layout.Overlay>
  )
}
