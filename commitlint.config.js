module.exports = {
  extends: ['@commitlint/config-conventional'],
  rules: {
    'type-enum': [
      2,
      'always',
      ['chore', 'fix', 'feat', 'style', 'test', 'revert', 'ci', 'docs', 'docs', 'refactor', 'env'],
    ],
  },
}
