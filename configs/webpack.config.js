const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')
const { merge } = require('webpack-merge')

const PROJECT_ROOT = path.resolve(__dirname, '../')
const APP_ENTRY = path.join(PROJECT_ROOT, 'src')
const PUBLIC_PATH = path.join(PROJECT_ROOT, 'public')
const DIST_PATH = path.join(PROJECT_ROOT, 'dist')
const DIST_FILENAME = `index.bundle.js`

const devConfig = require('./webpack.dev')
const prodConfig = require('./webpack.prod')

const assetPath = `/public`

const commonConfig = {
  entry: `${APP_ENTRY}/index.tsx`,
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx', '.json', '.css'],
    alias: {
      '@components': `${APP_ENTRY}/components`,
      '@actions': `${APP_ENTRY}/actions`,
      '@common_types': `${APP_ENTRY}/common_types`,
      '@locales': `${APP_ENTRY}/locales`,
      '@contexts': `${APP_ENTRY}/contexts`,
      '@hooks': `${APP_ENTRY}/hooks`,
      '@redux': `${APP_ENTRY}/redux`,
      '@utils': `${APP_ENTRY}/utils`,
      '@css': `${PUBLIC_PATH}/css`,
    },
  },
  output: {
    path: DIST_PATH,
    filename: DIST_FILENAME,
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.(ts|tsx)$/,
        use: ['babel-loader', 'ts-loader'],
        exclude: /node_modules/,
      },
      {
        test: /\.(png|jpeg|gif|svg|ttf|eof)(\?.*)?$/,
        include: /public/,
        use: [
          {
            loader: 'file-loader',
          },
        ],
        //}, {
        //  test: /\.json$/,
        //  loader: 'json-loader',
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      ASSET_PATH: assetPath,
    }),
    new HtmlWebpackPlugin({
      template: `${PUBLIC_PATH}/index.html`,
    }),
  ],
}

module.exports = (_, argv) => {
  if (argv.mode === 'development') {
    return merge(commonConfig, devConfig)
  }

  return merge(commonConfig, prodConfig)
}
