const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const config = {
  mode: 'production',
  devtool: 'hidden-source-map',
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: { importLoaders: 2 },
          },
        ],
      },
    ],
  },
}

module.exports = config
